var fs = require('fs');

config = {
  // cookieParser optional secret string
  cookie_secret: 'f5c223b6-6869-487a-bfc9-f7c925db3f3d',
  // mongoose uri
  // e.g. mongodb://username:server@mongoserver:10059/somecollection
  mongoose_uri: 'mongodb://localhost:27017/syslog',

  admin: {
    email: 'admin@example.org'
  },

  // http server
  server: {
    // accept incoming requests on a port, 3456 by default
    port: 3456,
    // accept incoming requests on ip address, 127.0.0.1 by default
    host: '127.0.0.1'
  },

  // https server
  secure_server: {
    // accept https requests on a port, 433 by default
    port: 3433,
    // accept https requests on ip address, any ip by default
    host: '0.0.0.0',
    // HTTPS Options
    tls: {
      // SSL Server Certificate:
      cert: fs.readFileSync('config/ssl/server.crt'),
      // Server Private Key:
      key: fs.readFileSync('config/ssl/server.key'),
      // A string of passphrase for the private key or null
      passphrase: null,
      // Certificate Authority (CA):
      ca: fs.readFileSync('config/ssl/cacrt.pem'),

      ciphers: 'ECDHE-RSA-AES256-SHA:AES256-SHA:RC4-SHA:RC4:HIGH:!MD5:!aNULL:!EDH:!AESGCM',
      honorCipherOrder: true,

      // Client Authentication: tls: true; local: false
      requestCert: true,
      // if true, server will reject any unauthorized connection
      // if false, server will fall back to local authn
      rejectUnauthorized: false,
    }
  },

  // Client Authentication: tls or local
  authn: 'tls'
}

module.exports = config;
