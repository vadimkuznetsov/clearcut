//
// createCappedCollections.js
//      creates messages capped collection, if it does not exists,
//      converts to capped, if it exists and is not capped,
//      ensures, that HOST field is indexed,
//      prints collection statistics
//
// Usage:
//      mongo --quiet syslog messages.js
//

// Connect to syslog db
//db = connect("localhost:27017/syslog")

if ( db.messages.exists() && !db.messages.isCapped() ) {
  db.runCommand({"convertToCapped": "messages", size:1000000000, max:500000, autoIndexId:true})
  db.messages.ensureIndex({_id:1})
}
else {
  db.createCollection("messages", {capped:true, size:1000000000, max:500000, autoIndexId:true})
}

// Ensure indexes
db.messages.ensureIndex({ts:1}, {background: true})
db.messages.ensureIndex({DATE:1, ts:-1}, {background: true})
db.messages.ensureIndex({HOST:1, ts:-1}, {background: true})
db.messages.ensureIndex({FACILITY:1, ts:-1}, {background: true})
db.messages.ensureIndex({PRIORITY:1, ts:-1}, {background: true})
db.messages.ensureIndex({PROGRAM:1, ts:-1}, {background: true})
//db.messages.ensureIndex({MESSAGE: "text"})

// Print collection stats in kilobytes
printjson( db.messages.stats(1024) )
