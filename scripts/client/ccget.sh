#!/bin/bash

#baseurl="https://clearcut:3433"
#cert="/home/user/.pki/user@email.crt"
#key="/home/user/.pki/user@email.key"

[ -z "${baseurl}" ] && echo "Clearcut URL is not set" && exit 1
[ -z "${cert}" ] && echo "TLS Certificate path is not set" && exit 1
[ -z "${key}" ] && echo "TLS Private Key path is not set" && exit 1

resource=${1}

curl -k --tlsv1 --cert "$(realpath ${cert})" --key "$(realpath ${key})" -H "Content-Type: application/json" ${baseurl}/${resource}
