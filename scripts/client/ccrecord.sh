#!/bin/bash

#baseurl="https://clearcut:3433"
#cert="/home/user/.pki/user@email.crt"
#key="/home/user/.pki/user@email.key"

[ -z "${baseurl}" ] && echo "Clearcut URL is not set" && exit 1
[ -z "${cert}" ] && echo "TLS Certificate path is not set" && exit 1
[ -z "${key}" ] && echo "TLS Private Key path is not set" && exit 1

hostname=$(hostname -s)
type="sysinfo"
status="success"
resource="record"

body=$(base64 -w0 < /dev/stdin)

data="{ \"hostname\" : \"${hostname}\", \"status\" : \"${status}\", \"type\" : \"${type}\", \"body\" : \"${body}\" }"
curl -k --tlsv1 --cert "$(realpath ${cert})" --key "$(realpath ${key})" -X POST -H "Content-Type: application/json" --data "${data}" "${baseurl}/${resource}"
