//
// host-discovery.js:
//    TODO
//
// Usage:
//    mongo --quiet mongos:27018/syslog host-discovery.js
//

// Connect to syslog db
//var db = connect("mongo1.vir:27017/syslog");

// Only Date, without time.
var today = new Date().toDateString();
print(today);

var h24 = Math.floor( Date.now() / 1000 ) - 86400

var hosts = db.messages.aggregate([
  {$match: {ts: {$gt: String(h24)}}},
  {$project: {HOST:1, SOURCEIP:1}},
  {$group: {_id: "$HOST", ips: {$addToSet: "$SOURCEIP"}}}
])
printjson(hosts.result)

hosts.result.forEach( function (h) {
  db.servers.update({hostname: h._id}, {$addToSet: {ips: {$each: h.ips}}}, {upsert: true})
});
