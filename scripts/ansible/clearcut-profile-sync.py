#!/usr/bin/python

import argparse
import ansible.runner
import sys
import json
import requests

"""
Updates clearcut server's profile using ansible setup module

"""

url = 'https://localhost:3433/server'
headers = {'content-type': 'application/json'}
cert = ('/home/user/.pki/user@email.crt',
        '/home/user/.pki/user@email.key')

parser = argparse.ArgumentParser(
    description='Pull server ansible_facts and save it')
parser.add_argument('hostname', action='store',
                    help='Hostname, groups or pattern')
parser.add_argument('-v', '--verbose', action='store_true',
                    help='verbose output')
args = parser.parse_args()

# construct the ansible runner and execute on host
try:
  results = ansible.runner.Runner(
    module_name='setup',
    module_args='',
    pattern=args.hostname,
    forks=10,
  ).run()
except ansible.errors.AnsibleError as e:
  print(e)
  sys.exit(1)

if results is None:
  print("ansible hosts file not found")
  sys.exit(1)

for (hostname, result) in results['contacted'].items():
  if not 'failed' in result:
    facts = results["contacted"][hostname]["ansible_facts"]
    try:
      r = requests.get(url+'/'+ hostname, headers=headers, cert=cert, verify=False)
    except requests.exceptions.RequestException as e:
      if args.verbose:
        print(e)
      print("can't connect to {0}".format(url))
      sys.exit(1)

    if r.status_code == 200:
      host = r.json()
      posturl = url + '/' + host["_id"]
    else:
      posturl = url
      host = {}
      host["hostname"] = hostname

    host["facts"] = facts
    host["fqdn"] =  facts["ansible_fqdn"]
    host["ips"] = facts["ansible_all_ipv4_addresses"] + facts["ansible_all_ipv6_addresses"]
    host["os"] = facts["ansible_distribution"] + ' ' + facts["ansible_distribution_version"]
    host["arch"] = facts["ansible_architecture"]

    try:
      r = requests.post(posturl, data=json.dumps(host), headers=headers, cert=cert, verify=False)
    except requests.exceptions.RequestException as e:
      if args.verbose:
        print(e)
      print("can't post facts")
      sys.exit(1)
    else:
      if args.verbose:
        print(r.status_code)
  else:
    print("failed to connect to {0}".format(hostname))
