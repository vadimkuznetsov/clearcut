#!/usr/bin/python

"""
Clearcut ansible external inventory script
==========================================



See http://ansible.github.com/api.html for more info


"""

import argparse
import os
import sys
import json
import requests
import time


# Ansible Clearcut inventory source:
url = 'https://localhost:3433/servers/ansible'
headers = {'content-type': 'application/json'}
cert = ('/home/user/.pki/user@email.crt',
        '/home/user/.pki/user@email.key')

inventory = dict()
inventory_path = "hosts.json"

parser = argparse.ArgumentParser(description='Pulls Ansible Inventory from Clearcut')
parser.add_argument('--list', action='store_true', help='List all the groups to be manged')
parser.add_argument('--host', action='store', help='Get all the host\'s variables')
parser.add_argument('--fresh', action='store_true',
                    help='Force refresh of cached hash from the source: '+url)
args = parser.parse_args()


def is_cache_valid(path, age=86400):
    """Is file exist and no older then X seconds, default 1 day."""
    if os.path.isfile(path) and time.time() - os.path.getmtime(path) < age:
        return True
    else:
        return False


def getfresh():
    """get new ansible inventory from source and save it"""
    try:
      r = requests.get(url, headers=headers, cert=cert, verify=False)
    except requests.exceptions.RequestException as e:
      print e
      sys.exit(1)

    inventory = r.json()
    with open(inventory_path, "wb") as fp:
        json.dump(inventory, fp, sort_keys=True, indent=2)
    return inventory


if args.fresh or not is_cache_valid(inventory_path):
    inventory = getfresh()
else:
    with open(inventory_path) as fp:
        inventory = json.load(fp)

if args.host:
    if args.host in inventory['_meta']['hostvars']:
        print(json.dumps(inventory['_meta']['hostvars'][args.host], sort_keys=True, indent=2))
    else:
        print("{}")
elif args.list:
    print(json.dumps(inventory, sort_keys=True, indent=2))
