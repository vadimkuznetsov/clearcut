# Folder public/js/ #

[Download Dojo](http://download.dojotoolkit.org/release-1.9.2/dojo-release-1.9.2.tar.gz)
and extract [Dojo Toolkit](http://dojotoolkit.org/) into this folder

[Download] (http://code.jquery.com/jquery-2.0.3.min.js) jQuery
[Download] (http://code.highcharts.com/highcharts.js) Highcharts JS

Or use these commands:

  wget -qO- http://download.dojotoolkit.org/release-1.9.2/dojo-release-1.9.2.tar.gz | tar xzf - --strip=1;
  wget http://code.jquery.com/jquery-2.0.3.min.js;
  wget http://code.highcharts.com/highcharts.js;
