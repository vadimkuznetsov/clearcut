//
// Clearcut
//      /servers client-side app
//

define([
  "dojo/query",
  "dojo/NodeList-dom",
  "dojo/dom-attr",
  "dojo/dom-class",
  "dojo/request"
  ],
  function(query, nodeListDom, domAttr, domClass, request) {
    "use strict";

    var fetchsorted = function(id){
      request.get("/server/?sort=" + id).then(function(results) {
        console.log("sorted by " + id);
        //console.log(results);
      }, function(err) {
        console.log("fetch error: /server/?sort=" + id);
      });
    };

    var initUI = function() {
      var sortby = domAttr.get("servers", "sortby");
      if ( sortby ) {
        if ( sortby.charAt(0) == '-') {
          domClass.add(sortby.substring(1), "descending");
        } else {
          domClass.add(sortby, "ascending");
        }
      }
      query("th").on("click", function(e) {
          var location = window.location;
          if(domClass.contains(e.target, "ascending"))
            location.search = "?sort=-" + e.target.id; //reverse order
          else
            location.search = "?sort=" + e.target.id;
      });
    };

    return {
      init: initUI
    };
  }
);
