//
// Clearcut
//      tabel filter
//

define(["dojo/dom-class",
       "dojo/on",
       "dojo/query",
       "dojo/keys",
       "dojo/dom-attr",
       "dojo/dom-style",
       "dojo/NodeList-manipulate"
  ],
  function(domClass, on, query, keys, domAttr, domStyle) {
    "use strict";

    var filter = function (node, evt) {
      var datatable = domAttr.get(node, "data-table")
      query("." + datatable + " tbody tr").forEach( function (row) {
        // Chrome supports innerText and textContent
        // FF does not support innerText
        // IE does not support textContent
        if ( row.textContent.toLowerCase().indexOf(node.value.toLowerCase()) === -1 )
          domStyle.set(row, "display", "none")
        else 
          domStyle.set(row, "display", "table-row")
      })
    }

    var init = function () {
      query(".table-filter").on("keyup", function (evt) {
        filter(this, evt)
      })
    }

    return {
      init: init
    }
  }
)
