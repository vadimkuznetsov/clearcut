//
// Clearcut messages.js
//      /(root) client-side app
//

define([
  "dojo/dom",
  "dojo/dom-construct",
  "dojo/dom-class",
  "dojo/dom-form",
  "dojo/request",
  "dojo/json",
  "dojo/on",
  "dojo/query",
  "dojo/NodeList-dom",
  "dojo/keys",
  "clearcut/alert"
  ],
  function(dom, domConstruct, domClass, domForm, request, JSON, on, query, nlDom, keys, alert ) {
    "use strict"

    var init = function() {
      domClass.add("filter", "hide")
      domClass.add("play-btn", "hide")
      domClass.add("reset-btn", "hide")
      domClass.add("clear-btn", "hide")
      domClass.add("url-btn", "hide")
      domClass.add("save-btn", "hide")
      query(".filter-btn").on("click", function (e) {
        domClass.toggle("filter", "hide")
        domClass.toggle("play-btn", "hide")
        domClass.toggle("reset-btn", "hide")
        domClass.toggle("clear-btn", "hide")
        domClass.toggle("url-btn", "hide")
        domClass.toggle("save-btn", "hide")
      })
      query("#play-btn").on("click", function (e) {
        dom.byId("filter-form").submit()
        return false
      })
      query("#reset-btn").on("click", function (e) {
        dom.byId("filter-form").reset()
        return false
      })
      query("#clear-btn").on("click", function (e) {
        query(".filter-form input[type='text']").forEach( function (node) {
          node.value = ""
        })
        return false
      })
      query("#url-btn").on("click", function (e) {
        var url = window.location.protocol + "//" + window.location.host + "/?";
        query(".filter-form input[type='text']").forEach( function (node) {
          url += node.id + "=" + node.value + "&"
        })
        url = url.substring(0, url.length - 1);
        alert.log(encodeURI(url))
        return false
      })
      query("#save-btn").on("click", function (e) {
        request.post("/filter", {
          data: domForm.toObject("filter-form") 
        }).then( function (result) { alert.log("Saved") }, function(err) { alert.log(err) })
      })
      query(".range-btn").on("click", function (e) {
        domClass.toggle("range-btn", "hide")
      })
    }

    return {
      init: init
    }
})
