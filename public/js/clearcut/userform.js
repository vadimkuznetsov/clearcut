// Clearcut
//      /user/name
//

define([
"dojo/query",
"dojo/NodeList-dom",
"dojo/dom-attr",
"dojo/dom-class",
"dojo/request"
],
function(query, nodeListDom, domAttr, domClass, request) {
  "use strict";

  var init = function () {
    query(".xhrdel").on("click", function(evt) {
      evt.preventDefault()
      console.log("Delete " + this.parentNode.id.value);
      request.del(this).then(function(results) {
        console.log("deleted " + results)
        window.location = "/user"
      }, function(err) {
        console.log("failed to delete " + this)
      })
    })
    query(".edit-btn").on("click", function (e) {
      domClass.toggle("edit-btn", "hide")
      domClass.toggle("submit-btn", "hide")
      query("#account-into input").forEach( function (node) {
        domAttr.remove(node, "readonly")
      })
    })
  }

  return {
    init: init
  }
})
  
