//
// Clearcut
//      dropdown menu
//

define(["dojo/dom-class",
        "dojo/on",
        "dojo/query",
        "dojo/NodeList-dom"
  ], function(domClass, on, query) {
    "use strict";

    var toggle = function (node, evt) {
      if (domClass.contains(node, "open")) {
        domClass.remove(node, "open")
      } else {
        query(".dropdown").removeClass("open")
        domClass.add(node, "open")
      }
    }

    var init = function () {
      query(".dropdown").on("click", function (evt) {
        toggle(this, evt)
      })
    }

    return {
      init: init
    }
  }
)
