define("clearcut/alert", ["dojo/dom","dojo/dom-class","dojo/on","dojo/query", "dojo/NodeList-manipulate",],
  function (dom, domClass, on, query) {
    "use strict"

    function log (msg) {
      query("#msg").text(msg)
      domClass.remove("alert", "hide")
      var timeout = setTimeout(function() {
        domClass.add("alert", "hide")
      }, 30000)
      return this
    }

    query("#alert #close-btn").on("click", function (e) {
      domClass.add("alert", "hide")
    })

    return {
      log: log
    }
})
