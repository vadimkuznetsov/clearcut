// Clearcut
//      /server/name
//

define([
"dojo/query",
"dojo/NodeList-dom",
"dojo/dom-attr",
"dojo/dom-class",
"dojo/request"
],
function(query, nodeListDom, domAttr, domClass, request) {
  "use strict";

  var init = function () {
    query("#decommission").on("click", function(evt) {
      evt.preventDefault();
      console.log("Decomission " + this.parentNode.id.value);
      request.del(this).then(function(results) {
        console.log("deleted " + results);
        window.location = "/server";
      }, function(err) {
        console.log("failed to delete " + this);
      });
    });
  };

  return {
    init: init
  };

});
