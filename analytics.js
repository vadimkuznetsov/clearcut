#!/usr/bin/env node

//
// Collects daily stats
//

var argv = require('optimist')
  .usage('Collects daily analytics.\nUsage: $0')
  .alias('i', 'interval')
  .describe('i', 'setInterval delay in milliseconds; Default: every hour.')
  .default('i', 3600000)
  .boolean('r').alias('r', 'reset')
  .describe('r', 'Reset (drop) analytics collections.')
  .argv;

var mongoose = require('mongoose')
var config   = require('./config')
var Message  = require('./models/message')
var Analytics = require('./models/analytics')

var db = mongoose.connect(config.mongoose_uri)

function analytics () {
  var now = Math.floor( Date.now() / 1000 )
  Analytics.findOneAndUpdate({_id: "lastrun"}, {lastrun: String(now)}, {new:false, upsert:true, select:"lastrun"}, function(err, since) {
    if (!since || !since.lastrun) {
      since = {}
      since.lastrun = "0"
    }

    var o = {}

    o.map = function () {
      var ts = this._id.getTimestamp()
      ts.setHours(0,0,0,0)
      var d = ts.getTime()
      var key = {
        h: this.HOST,
        f: this.FACILITY,
        l: this.PRIORITY,
        p: this.PROGRAM,
        d: d
      }
      emit(key, 1)
    }

    o.reduce = function (key, values) {
      var reducedValue = 0
      values.forEach(function(v) {
        reducedValue += v
      })
      return reducedValue
    }

    o.query = { ts: { $gte: since.lastrun, $lt: now } }
    o.verbose = true      // default: true
    o.out = { reduce: 'analytics' }

    Message.mapReduce(o, function (err, Daily, stats) {
      console.log('Daily map reduce took %d ms', stats.processtime)
    })
  })
}

if ( argv.r ) {
  Analytics.remove( function (err) {
    console.log('Daily analytics is dropped.')
    process.exit(0)
  })
}
else {
  analytics()
  setInterval(analytics, argv.interval)
}
