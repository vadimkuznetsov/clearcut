//
//
//

var crypto  = require('crypto')


function genSalt(size, callback) {
  if ( 'function' == typeof size ) {
    callback = size, size = 64
  }
  crypto.randomBytes(size, function(err, bytes) {
    if (err) { return callback(err) }
    callback( null, bytes.toString('base64'))
  })
}

exports.genSalt = genSalt

function genHash (pass, salt, callback) {
  crypto.pbkdf2(pass, salt, 1000, 64, function (err, key) {
    if (err) { callback(err) }
    callback(null, { hash: key.toString('base64'), salt: salt })
  })
}

// hash a password
exports.genPass = function genPass (pass, callback) {
  genSalt( function (err, salt) {
    if (err) { callback(err) }
    return genHash(pass, salt, callback)
  })
}

exports.compare  = function compare (plaintext, pass, callback) {
  genHash(plaintext, pass.salt, function (err, check) {
    if (err) { callback(err) }
    return callback(null, pass.hash == check.hash)
  })
}
