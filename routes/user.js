//routes/users.js
//
// User management routes
//

var User    = require('../models/user')
var Auth    = require('../models/auth')
var config  = require('../config')
var passwd  = require('../lib/passwd')

function authenticate (req, res, next) {
  if ( config.authn === 'tls')
    tlsAuthenticate(req, res, next)
  else {
    req.session.error = 'Access denied!'
    res.redirect('/login')
  }
}

function tlsAuthenticate (req, res, next) {
  var subject = {}
  var user_data = {}

  if ( req.client.authorized ) {
    subject = req.connection.getPeerCertificate().subject
    user_data = {
      //     user:    subject.CN,
      email:    subject.emailAddress,
      org:      subject.O,
      ou:       subject.OU,
      location: subject.L,
      state:    subject.ST,
      country:  subject.C,
      admin:    false,
      locked:   false
    }

  } else if ( req.secure && req.header('x-ssl-client-verified') === 'SUCCESS' ) {
    subject = getPeerSubject(req.header('x-ssl-client-dn'))
    user_data = {
      //     user:    subject.CN,
      email:    subject.emailAddress,
      org:      subject.O,
      ou:       subject.OU,
      location: subject.L,
      state:    subject.ST,
      country:  subject.C,
      admin:    false,
      locked:   false
    }

  } else if ( req.ip === '127.0.0.1' ) {
    user_data = {
      username: "admin",
      fname:    "Clearcut",
      lname:    "Admin",
      email:    config.admin.email,
      admin:    true,
      locked:   false
    }
  } else {
    return res.redirect('/login')
  }

  User.findOne({email: user_data.email}, function (err, user) {
    if ( err ) {
      console.log(err)
    }
    // Auto create
    if ( !user ) user = new User(user_data)
    user.lastlog = new Date()
    user.save(function (err) {
      if ( err ) {
        console.log(err)
        next(err)
      }
    })
    req.session.user_id = user._id
    res.locals.user = user
    next()
  })
}

function getPeerSubject(dn) {
  var obj = {}
  dn.split('/').forEach( function (item) {
    var pair = item.split('=')
    obj[pair[0]] = pair[1];
  })
  return obj
}

exports.requireAuthentication = function (req, res, next) {
  if (req.session.user_id) {
    User.findById(req.session.user_id, function (err, user) {
      if (err) console.log(err)
      if (user) {
        res.locals.user = user
        next()
      } else {
        req.session.user_id = null
        authenticate(req, res, next)
      }
    })
  } else {
    authenticate(req, res, next)
  }
}

exports.requireAuthorization = function (req, res, next) {
  if (res.locals.user.locked)
    res.redirect('/login')
  else
    next()
}

exports.requireAdmin = function (req, res, next) {
  if (req.session.user_id)
    User.findById(req.session.user_id, function (err, user) {
      if (err) console.log(err)
      if (user.admin) next()
      else unauthorized(req, res)
    })
}

// GET /unauthorized
function unauthorized (req, res) {
  res.render('unauthorized', { title: 'Unauthorized!' })
}

// GET /login
exports.login = function (req, res) {
  return res.render('login')
}

// POST /login
exports.signin = function (req, res, next) {
  var name = req.body.username
  var pass = req.body.password
  if ( !name ) {
    return res.render('login', { invalidUsername: true})
  }
  if ( !pass ) {
    return res.render('login', { invalidPassword: true})
  }
  User.findOne({username: name}, function (err, user) {
    if (err) { return next(err) }
    if (!user || !user.password || !user.password.salt || !user.password.hash) {
      return res.render('login', { invalid: "The username or password you entered is incorrect." }) 
    }

    passwd.compare(pass, user.password, function (err, result) {
      if (!result) {
        return res.render('login', { invalid: "The username or password you entered is incorrect." }) 
      }
      user.lastlog = new Date()
      user.save(function (err) {
        if ( err ) { console.log(err) }
      })

      req.session.user_id = user._id
      res.redirect('/')
    })
  })
}

// GET /logout
exports.logout = function (req, res) {
  req.session.user_id = null
  res.redirect('/')
}

// GET /account
exports.account = function (req, res) {
  User.findById(req.session.user_id, function (err, user){
    res.render('account', {account: user, readonly: true})
  })
}

// POST /account
exports.accountUpdate = function (req, res) {
  User.findByIdAndUpdate(req.session.user_id, req.body, function (err, user) {
    if ( err )
      console.log(err)
    res.render('account', {account: user, readonly: true})
  })
}

// DELETE /account
exports.accountDelete = function (req, res) {
  User.findByIdAndRemove(req.session.user_id, function (err) {
    delete req.session.user_id
    delete req.session.query
    res.redirect('/')
  })
}

// GET /changePassword
exports.editPassword = function (req, res) {
  User.findById(req.session.user_id, function (err, user) {
    if (err) { return next(err) }
    return res.render('change-password', {account: user})
  })
}

// POST /changePassword
exports.changePassword = function (req, res) {
  var current = req.body.currentPassword
  var newpass = req.body.newPassword

  if ( !current ) {
    return res.render('change-password', { invalidPassword: true})
  }
  if ( !newpass ) {
    return res.render('change-password', { invalidNewPassword: true})
  }
  if ( newpass != req.body.confirmPassword ) {
    return res.render('change-password', { invalid: "Passwords do not match."})
  }
  User.findById(req.session.user_id, function (err, user) {
    if (err) { return next(err) }
    if (!user || !user.password || !user.password.salt || !user.password.hash) {
      return res.render('change-password', { invalid: "The username or password you entered is incorrect." }) 
    }
    passwd.compare(current, user.password, function (err, result) {
      if (!result) {
        return res.render('change-password', { invalidPassword: "The current password you entered is incorrect." }) 
      }
      passwd.genPass(newpass, function (err, pass) {
        user.password = pass
        user.password.changed = new Date()
        user.save(function (err) {
          if ( err ) { console.log(err) }
        })
        return res.redirect('/account')
      })
    })
  })
}

// POST /filter
exports.filterUpdate = function (req, res) {
  console.log(req.body)
  var today = new Date().toString()
  User.findByIdAndUpdate(req.session.user_id, { $push: {filters: { name: today, filter: JSON.stringify(req.body) } }}, function (err, user) {
    if ( err ) {
      console.log(err)
      res.send(500, err)
    } else {
      res.send(200)
    }
  })
}

// DELETE /filter
exports.filterDeleteAll = function (req, res) {
  User.findByIdAndUpdate(req.session.user_id, { $set: {filters: [] }}, function (err, user) {
    if ( err ) {
      console.log(err)
      res.send(500, err)
    } else {
      res.send(200)
    }
  })
}

// DELETE /filter/:id
exports.filterDelete = function (req, res) {
  User.findByIdAndUpdate(req.session.user_id, { $pull: {filters: { _id: req.params._id} }}, function (err, user) {
    if ( err ) {
      console.log(err)
      res.send(500, err)
    } else {
      res.send(200)
    }
  })
}

// GET /user
exports.users = function (req, res) {
  var conditions = {}
  var sortby = 'lname' // space delimited list of path names
  if ( req.query.sort )
    sortby = req.query.sort

    User.find(conditions).sort(sortby).find(function (err, users) {
      if ( err ) {
        console.log(err)
      } else {
        if ( req.xhr ) {
          res.json(users)
        } else {
          res.render('users', { users: users, sortby: sortby})
        }
      }
    })
}

// GET /user/:id
exports.userbyid = function (req, res) {
  User.findById(req.params.id, function (err, user) {
    if ( err ) console.log(err)
    else if (user == null)
      res.redirect('/user')
    else {
      res.render('user-edit', { account: user, readonly: false })
    }
  })
}

// POST /user/:id
exports.update = function (req, res) {
  console.log(req.body)
  if ( req.body.admin && req.body.admin === 'true') 
    req.body.admin = true
  else 
    req.body.admin = false
  if ( req.body.locked && req.body.locked === 'true') 
    req.body.locked = true
  else 
    req.body.locked = false
  console.log(req.body)
  User.findByIdAndUpdate(req.body.id, req.body, function (err, user) {
    if ( err ) 
      console.log(err)
    if (res.locals.user.admin)
      res.redirect('/user')
    else
      res.redirect('/')
  })
}

// DELETE /user/:id
exports.delete = function (req, res) {
  if (req.params.id) {
    User.findByIdAndRemove(req.params.id, function (err) {
      if ( err ) {
        console.log(err)
        res.send(404, err)
      } else {
        res.send(204)
      }
    })
  }
}
