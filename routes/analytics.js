//routes/analytics.js
//
//
var mongoose  = require('mongoose')
var Message   = require('../models/message')
var Analytics = require('../models/analytics')


exports.daily = function (req, res) {
  var today = new Date().setUTCHours(0,0,0,0)   //returns time in miliseconds
  var monthago = today - 2592000000             // 30 days
  
  var o = {}
  
  o.map = function () {
    var key = {
      d: this._id.d
    }
    emit(key, this.value)
  }
  
  o.reduce = function (key, values) {
    var reducedValue = 0
    values.forEach(function(v) {
      reducedValue += v
    })
    return reducedValue
  }
  o.query = { "_id.d": { $gte: monthago } }
  o.verbose = true

  Analytics.mapReduce(o, function (err, results, stats) {
    console.log('Total map reduce took %d ms', stats.processtime)
    console.log('Last 30 days search returned %d rows', results.length)
    var data = results.map( function(item) { return [item._id.d, item.value]})
    if ( req.is('application/json') || req.xhr ) {
      return res.json(data)
    }
    res.render('analytics', { series: [{name: 'Messages daily', data: data}] })
  })
}

exports.bylevel = function (req, res) {
  var today = new Date().setUTCHours(0,0,0,0)   //returns time in miliseconds
  var monthago = today - 2592000000             // 30 days
  
  var o = {}
  
  o.map = function () {
    var key = {
      d: this._id.d,
      l: this._id.l,
    }
    emit(key, this.value)
  }
  
  o.reduce = function (key, values) {
    var reducedValue = 0
    values.forEach(function(v) {
      reducedValue += v
    })
    return reducedValue
  }
  o.query = { "_id.d": { $gte: monthago } }
  o.verbose = true
  
  Analytics.mapReduce(o, function (err, results, stats) {
    console.log('Level map reduce took %d ms', stats.processtime)
    console.log('Last 30 days search returned %d rows', results.length)
    var levels = [ "debug", "info", "notice", "warn", "err", "crit", "alert", "emergency" ]
    var series = levels.map( function(level) { 
      var data = results
        .filter( function(item) { return item._id.l === level })
        .map( function(item) { return [item._id.d, item.value] })      
      return { name: level, data: data }
    })
    if ( req.is('application/json') || req.xhr ) {
      return res.json(series)
    }
    res.render('analytics', { series: series })
  })
}
