//routes/message.js
//
// Messages Routes
//

var util    = require('util')
var async   = require('async')
var Message = require('../models/message')
var Server  = require('../models/server')

if ( !Date.prototype.toFormatString ) {
  ( function() {

    function pad(number) {
      var r = String(number);
      if ( r.length === 1 ) {
        r = '0' + r;
      }
      return r;
    }

    Date.prototype.toFormatString = function() {
      return this.getFullYear()
      + '-' + pad( this.getMonth() + 1 )
      + '-' + pad( this.getDate() )
      + ' ' + pad( this.getHours() )
      + ':' + pad( this.getMinutes() );
    };

  }() );
}

if ( !Date.prototype.getMonthName ) {
  ( function() {
    Date.prototype.monthNames = [
      "January", "February", "March",
      "April", "May", "June",
      "July", "August", "September",
      "October", "November", "December"
    ]

    Date.prototype.getMonthName = function () {
      return this.monthNames[this.getMonth()]
    }
    Date.prototype.getShortMonthName = function () {
      return this.getMonthName().substr(0, 3)
    }

  }() )
}

// Merge the contents of two or more objects together into the first object.
function mixin (dest, sources) {
  var name, source, s, i, j, empty = {};

  if (!dest) dest = {}
  for (i = 1, l = arguments.length; i < l; i++) {
    source = arguments[i]
    for(name in source) {
      // the (!(name in empty) || empty[name] !== s) condition avoids copying properties in "source"
      // inherited from Object.prototype.      For example, if dest has a custom toString() method,
      // don't overwrite it with the toString() method that source inherited from Object.prototype
      s = source[name]
      if (!(name in dest) || (dest[name] !== s && (!(name in empty) || empty[name] !== s))) {
        dest[name] = s
      }
    } 
  }
  return dest
}

function isEmpty( obj ) {
  for ( var p in obj ) { 
    if ( obj.hasOwnProperty( p ) ) { return false }
  }
  return true
}

var keys = ["DATE", "HOST", "FACILITY", "PRIORITY", "PROGRAM", "MESSAGE"]


// from mongodb query to input string
function fromQuery(query) {
  if ( typeof query === "string" ) {
    return query
  }
  if ( typeof query === "object" ) {
    if ( query.hasOwnProperty("$ne") ) {
      return '!' + query["$ne"].toString()
    }
    if ( query.hasOwnProperty("$in") ) {
      return '[' + query["$in"].toString() + ']'
    }
    if ( query.hasOwnProperty("$nin") ) {
      return '![' + query["$nin"].toString() + ']'
    }
    if ( query.hasOwnProperty("$regex") ) {
      return '/' + query["$regex"].toString() + '/'
    }
  }
  if ( typeof query === "undefined" ) {
    return ''
  }
}

// to input form fields 
function toInput(query) {
  var input = {}
  keys.forEach( function (key) {
    input[key.toLowerCase()] = fromQuery(query[key])
  })
  return input
}


// GET /
// GET /?page=0
// GET /?host=hostname
// GET /?facility=daemon&priority=err
// GET /?message="-- MARK --"
// POST /
//     date=val&host=val&facility=val&priority=val&program=val&message=val
//
// GET /owner/:owner

// parse and validate url query, user input; save query in session
exports.params = function (req, res, next) {
  var dateString, rangeString

  if ( !req.session.hasOwnProperty("query") ) {
    req.session.query = {}
  }
  if ( !req.session.hasOwnProperty("range") ) {
    req.session.range = { interval: 259200 }      // 3 days
  }

  var query = req.session.query; // session query shortcut
  var range = req.session.range; // session range shortcut

  // process date=val&host=val&facility=val&priority=val&program=val&message=val
  keys.forEach( function (key) {
    var ss, str = req.param(key.toLowerCase())
    if ( str ) {
      if ( ss = str.match(/^\/(.+)\/$/) ) {
        query[key] = {"$regex": ss[1]}
      }
      else if ( ss = str.match(/^\[(.+)\]$/) ) {
        query[key] = {"$in": ss[1].split(/,/)}
      }
      else if ( ss = str.match(/^\!\[(.+)\]$/) ) {
        query[key] = {"$nin": ss[1].split(/,/)}
      }
      else if ( ss = str.match(/^\!([\w\s.~!@#$%^&*()\-+=,.;:"'/\\{}\[\]()<>\?]+)$/)) {
        query[key] = {"$ne": ss[1]}
      }
      else if ( str.match(/^[\w\s.~!@#$%^&*()\-+=,.;:"'/\\{}\[\]()<>\?]+$/)) {
        query[key] = str
      }
      else
        console.log("no match: " + str)
    }
    else if ( str === '' && query.hasOwnProperty(key) ) {
      delete query[key]
    }
  })
  // parse start, end and range
  if ( dateString = req.param("start")) {
    range.start = Math.floor(Date.parse(dateString) / 1000)
  }
  else if (dateString === '') {
    range.start = 0
  }
  if ( dateString = req.param("end")) {
    range.end = Math.floor(Date.parse(dateString) / 1000)
  }
  else if (dateString === '') {
    range.end = 0
  }
  if ( rangeString = req.param("range")) {
    try {
      range.interval = rangeString.toLowerCase()
      .match(/(\d+)(w|d|h|m)/ig)
      .map( function(item) {
        var rst = item.match(/(\d+)(w|d|h|m)/)
        switch (rst[2]) {
          case "w":                     // weeks
            return rst[1] * 604800
          case "d":                     // days
            return rst[1] * 86400
          case "h":                     // hours
            return rst[1] * 3600
          case "m":                     // minutes
            return rst[1] * 60
        }
      })
      .reduce(function(prev, curr, index, array){
        return prev + curr;
      })
    } catch (e) {
      // statements to handle TypeError exceptions
      console.log("invalid input: " + JSON.stringify(req.query))
    }
  }
  else if ( rangeString === '' ) {
    range.interval = 0
  }
  // make query out of range
  if (range.start) {
    if (typeof query.ts === "undefined") {
      query.ts = {}
    }
    query.ts['$gte'] = range.start
    if (range.end) {
      query.ts['$lt'] = range.end
    }
    else if (range.interval) {
      query.ts['$lt'] = range.start + range.interval
    }
    else {
      delete query.ts['$lt']
    }
  }
  else if (range.end) {
    if (typeof query.ts === "undefined") {
      query.ts = {}
    }
    query.ts['$lt'] = range.end
    if (range.interval) {
      query.ts['$gte'] = range.end - range.interval
    }
    else {
      delete query.ts['$gte']
    }
  } 
  else if (range.interval) {
    var now = Math.floor( Date.now() / 1000 )
    query['ts'] = { $gte: now - range.interval, $lt: now }
  }
  else {
    delete query["ts"]
  }
  // parse owner
  if ( req.param("owner") ) {
    Server.find({owners: req.param("owner")}, 'hostname', function(err, hosts) {
      if ( err ) {
        console.log(err)
        next(err)
      } 
      if ( hosts.length != 0 ) {
        var hostnames = hosts.map(function(h) { return h.hostname })
        query.HOST = { "$in": hostnames }
      }
      next()
    })
  }
  else
    next()
}

// GET /
exports.index = function (req, res) {
  var conditions = req.session.query
//   console.log(util.inspect(req.session, {depth:5}))

  async.waterfall([
    // message count
    function (callback) {
      var page = 0
      if ( req.query.page ) {
        page = parseInt(req.query.page)
        if ( isNaN(page) ) {
          page = 0 // last
        }
      }
      var rpp = 25
      if ( req.query.rpp ) {
        rpp = parseInt(req.query.rpp)
        if ( isNaN(rpp) || rpp < 0 ) {
          rpp = 25
        }
      }
      var locals = {
        info: '',
        messages: [],
        page: page,
        count: 0,
        limit: rpp,
        pages: 1,
        prevpage: 1,
        nextpage: 1,
        range: req.session.range
      }

      Message.count(conditions, function (err, count) {
        locals.count = count
        if ( locals.limit == 0 ) {
          locals.pages = 1
        }
        else {
          locals.pages = Math.ceil(locals.count / locals.limit)
        }
        if ( locals.count == 0 ) {
          locals.pages = 1
        }
        locals.page %= locals.pages
        if ( locals.page == 0 ) {
          locals.page = locals.pages
        }
        locals.prevpage = (locals.page == 1) ? locals.pages : locals.page - 1
        locals.nextpage = (locals.page == locals.pages) ? 1 : locals.page + 1
        callback(null, conditions, locals)
      })
    },
    // find messages
    function (conditions, locals, callback) {
      var skip = (locals.pages - locals.page) * locals.limit
      Message.find(conditions).sort('-ts').skip(skip).limit(locals.limit).exec(function (err, messages) {
        if ( err ) {
          console.log(err)
          locals.info = err
          locals.messages = []
        } else {
          locals.messages = messages
        }
        locals.query = toInput(conditions)
        callback(null, locals)
      })
    }],
    function (err, result) {
      if ( err ) {
        console.log(err)
        result.info = "Error: " + err
      }
      if ( req.is('application/json') ) {
        res.json(result)
      }
      else {
        res.render('index', result)
      }
    }
  )
}

// GET /id/:id
exports.id = function (req, res) {
  var id
  if ( !req.params.id ) {
    res.redirect('/')
  } else {
    id = req.params.id
  }
  var conditions = { _id: id }
  Message.find(conditions, function (err, messages) {
    if ( req.xhr ) {
      res.json(messages)
    } else {
      var locals = {
        info: '',
        count: 1,
        messages: [],
        page: 1,
        pages: 1,
        prevpage: 1,
        nextpage: 1,
        query: toInput(conditions)
      }
      if ( err ) {
        console.log(err)
        locals.count = 0
        locals.info = err
      } else {
        locals.messages = messages
      }
      res.render('index', locals)
    }
  })
}

// GET /all
exports.all = function (req, res, next) {
  req.session.query = {}
  req.session.range = { interval: 259200 }      // 3 days
  if ( req.is('application/json') ) {
    return next()
  }
  else {
    return res.redirect('/')
  }
}

// GET /today
exports.today = function (req, res, next) {
  if ( !req.session.hasOwnProperty("range") ) {
    req.session.range = {}
  }
  var day = new Date()
  day.setHours(0,0,0)
  req.session.range.start = Math.floor(day.getTime() / 1000)
  day.setDate(day.getDate() + 1)
  req.session.range.end = Math.floor(day.getTime() / 1000)
  if ( req.is('application/json') ) {
    return next()
  }
  else {
    return res.redirect('/')
  }
}

// GET /yesterday
exports.yesterday = function (req, res, next) {
  if ( !req.session.hasOwnProperty("range") ) {
    req.session.range = {}
  }
  var day = new Date()
  day.setHours(0,0,0)
  req.session.range.end = Math.floor(day.getTime() / 1000)
  day.setDate(day.getDate() - 1)
  req.session.range.start = Math.floor(day.getTime() / 1000)
  if ( req.is('application/json') ) {
    return next()
  }
  else {
    return res.redirect('/')
  }
}

// GET /failedauth
exports.failedauth = function (req, res, next) {
  req.session.query = mixin(req.session.query, {"PRIORITY":{"$in":["err","crit","alert","emergency"]}, "FACILITY":{"$in":["auth","authpriv"]}})
  if ( req.is('application/json') ) {
    return next()
  }
  else {
    return res.redirect('/')
  }
}

// GET /critical
exports.critical = function (req, res, next) {
  req.session.query = mixin(req.session.query, {"PRIORITY":{"$in":["crit","alert","emergency"]}})
  if ( req.is('application/json') ) {
    return next()
  }
  else {
    return res.redirect('/')
  }
}
