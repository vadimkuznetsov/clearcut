//
// Server routes
//

var Server  = require('../models/server')
var Message = require('../models/message');

// Array difference
// Returns a new array, that is copy of A array, removing any items that also appear in array B
// A and B are sorted arrays
function diff(A, B) {
  var d = []
  var lastidx = 0
  A.forEach( function (a) {
    var idx = B.indexOf(a, lastidx)
    if ( idx != -1 ) {
      lastidx = idx
    }
    else {
      d.push(a)
    }
  })
  return d
}

// Splits string into array and trims members
function trimarray (a) {
  if ('string' === typeof a)
    a = a.split(",")
  return a.map( function (s) {
    return s.trim().toLowerCase()
  }).sort()
}

function sanitize (s) {
  if ('string' === typeof s) {
    return s.trim().replace(/[^ _A-Za-z0-9-@.,]/g, '').substring(0,256)
  }
  else {
    return ''
  }
}

// GET /server
// GET /server/?sort=owners%20hostname
// GET /server/?hostname=&ips=&networkZone=&os=&arch=&owner=&class=&envType=&loc=
// GET /server/?fields=hostname,ips
exports.servers = function (req, res) {
  var query = Server.find()

  if ( req.query.hostname ) {
    query.where('hostname').equals(req.query.hostname)
  }
  if ( req.query.ips ) {
    query.where('ips').equals(req.query.ips)
  }
  if ( req.query.networkZone ) {
    query.where('networkZone').equals(req.query.networkZone)
  }
  if ( req.query.os ) {
    query.where('os').equals(req.query.os)
  }
  if (req.query.arch ) {
    query.where('arch').equals(req.query.arch)
  }
  if (req.query.owners ) {
    query.where('owners').equals(req.query.owners)
  }
  if (req.query.groups ) {
    query.where('groups').equals(req.query.groups)
  }
  if (req.query.class ) {
    query.where('class').equals(req.query.class)
  }
  if (req.query.envType ) {
    query.where('envType').equals(req.query.envType)
  }
  var sortby = 'hostname' // space delimited list of path names
  if ( req.query.sort ) {
    sortby = req.query.sort
  }
  query.sort(sortby)
  if ( req.query.fields ) {
    query.select(req.query.fields)
  }
  query.find( function (err, servers) {
    if ( err ) {
      console.log(err)
      return res.send(500, err)
    }
    if ( req.is('application/json') ) {
      return res.json(servers)
    }
    else if ( req.is('text/plain') ) {
      res.set('Content-Type', 'text/plain')
      return res.send(servers.map( function (s) { return s.hostname } ))
    }
    else {
      return res.render('servers', { servers: servers, sortby: sortby })
    }
  })
}

// GET /server/:host
exports.serverByName = function (req, res) {
  var host = sanitize(req.params.host)
  Server.findOne({ hostname: host }, function (err, server) {
    if ( err ) {
      console.log(err)
      if ( req.is('application/json') || req.xhr ) {
        return res.send(500, {error: err})
      }
      else {
        return res.render('server', { info: err })
      }
    }
    if ( !server ) {
      if ( req.is('application/json') || req.xhr ) {
        return res.send(404, req.params.host + " not found\n")
      }
      server = {
        _id: '',
        hostname: '',
        fqdn: '',
        ips: [],
        os: '',
        arch: '',
        desc: '',
        envType: '',
        networkZone: '',
        owners: [],
        groups: [],
        class: '',
        status: '',
        loc: ''
      }
    }
    if ( req.is('application/json') || req.xhr ) {
      return res.json(server)
    }
    else {
      return res.render('server', { server_data: server })
    }
  })
}


// POST /server
exports.createServer = function (req, res) {
  server = new Server()
  if ( typeof req.body.hostname !== 'undefined' ) {
    server.hostname = req.body.hostname
  }
  else {
    if ( req.is('application/json') || req.xhr ) {
      return res.send(500, "Hostname is required")
    }
    else {
      return res.render('server', { info: "Hostname is required" })
    }
  }
  if ( typeof req.body.fqdn !== 'undefined' ) server.fqdn = trimarray(req.body.fqdn)
  if ( typeof req.body.ips !== 'undefined' )  server.ips = trimarray(req.body.ips)
  if ( typeof req.body.os !== 'undefined' ) server.os = req.body.os
  if ( typeof req.body.arch !== 'undefined' ) server.arch = req.body.arch
  if ( typeof req.body.desc !== 'undefined' ) server.desc = req.body.desc
  if ( typeof req.body.envType !== 'undefined' ) server.envType = req.body.envType
  if ( typeof req.body.networkZone !== 'undefined' ) server.networkZone = req.body.networkZone
  if ( typeof req.body.owners !== 'undefined' ) server.owners = trimarray(req.body.owners)
  if ( typeof req.body.groups !== 'undefined' ) server.groups = trimarray(req.body.groups)
  if ( typeof req.body.class !== 'undefined' ) server.class = req.body.class
  if ( typeof req.body.status !== 'undefined' ) server.status = req.body.status
  if ( typeof req.body.loc !== 'undefined' ) server.loc = req.body.loc
  if ( typeof req.body.facts !== 'undefined' ) server.facts = req.body.facts

  server.save( function (err, saved, nc) {
    if ( err ) {
      console.log(err)
      return res.render('server', { info : err, server : server })
    }
    if ( req.is('application/json') || req.xhr ) {
      return res.json(saved)
    }
    else {
      return res.redirect('/server')
    }
  })
}

// POST /server/:id
exports.updateServer = function (req, res) {
  Server.findOne({_id: req.params.id}, function (err, server) {
    if (err) {
      console.log(err)
      if ( req.is('application/json') || req.xhr ) {
        return res.send(500, {error: err})
      }
      else {
        return res.render('server', { info: err })
      }
    }
    if (!server) {
      if ( req.is('application/json') || req.xhr ) {
        return res.send(404, { error: req.params.id + " not found" } )
      }
      else {
        return res.render('server', { info: req.params.id + " not found" })
      }
    }
    if ( typeof req.body.hostname !== 'undefined' ) server.hostname = req.body.hostname
    if ( typeof req.body.fqdn !== 'undefined' ) server.fqdn = trimarray(req.body.fqdn)
    if ( typeof req.body.ips !== 'undefined' )  server.ips = trimarray(req.body.ips)
    if ( typeof req.body.os !== 'undefined' ) server.os = req.body.os
    if ( typeof req.body.arch !== 'undefined' ) server.arch = req.body.arch
    if ( typeof req.body.desc !== 'undefined' ) server.desc = req.body.desc
    if ( typeof req.body.envType !== 'undefined' ) server.envType = req.body.envType
    if ( typeof req.body.networkZone !== 'undefined' ) server.networkZone = req.body.networkZone
    if ( typeof req.body.owners !== 'undefined' ) server.owners = trimarray(req.body.owners)
    if ( typeof req.body.groups !== 'undefined' ) server.groups = trimarray(req.body.groups)
    if ( typeof req.body.class !== 'undefined' ) server.class = req.body.class
    if ( typeof req.body.status !== 'undefined' ) server.status = req.body.status
    if ( typeof req.body.loc !== 'undefined' ) server.loc = req.body.loc
    if ( typeof req.body.facts !== 'undefined' ) server.facts = req.body.facts

    server.save( function (err, server, nc) {
      if ( err ) {
        console.log(err)
        return res.render('server', { info: err, server: server })
      }
      if ( req.is('application/json') || req.xhr ) {
        return res.json(server)
      }
      else {
        return res.redirect('/server')
      }
    })
  })
}

// DELETE /server/:id
exports.deleteServer = function (req, res) {
  if(req.params.id){
    Server.findByIdAndRemove(req.params.id, function (err) {
      if ( err ) {
        console.log(err)
        res.send(404, err)
      }
      else {
        res.send(204)
      }
    })
  }
}

// GET /servers/supported
exports.supported = function (req, res, next) {
  var h24 = Math.floor( Date.now() / 1000 ) - 43200     // last 12 hours

  Message.aggregate([
    {$match: {ts: {$gt: String(h24)}}},
    {$project: {HOST:1, ts:1}},
    {$sort: {ts: -1}},
    {$group: {_id: "$HOST", lastSeen: {$first: "$ts"}}},
    {$project: {_id: 0, hostname: "$_id", lastSeen: 1}},
    {$sort: {hostname: 1}}],
    function (err, hosts) {    // result is an array
      if ( err ) {
        console.log(err)
        return next(err)
      }
      var seen = hosts.map( function (h) { return h.hostname })
      Server.find({status: { $ne: "unsupported" }}).sort('hostname').select('hostname').exec(function (err, servers) {
        if ( err ) {
          console.log(err)
          return next(err)
        }
        var known = servers.map( function (h) { return h.hostname })
        var recent = diff(seen, known)
        var lost = diff(known, seen)
        if ( req.is('application/json') || req.xhr ) {
          res.json({ seen: seen, recent: recent, lost: lost })
        }
        else {
          res.render('servers-supported', { seen: seen, recent: recent, lost: lost })
        }
      })
    }
  )
}

// GET /servers/owners
exports.owners = function (req, res, next) {
  Server.aggregate([
    {$sort: { owners: -1}},
    {$unwind: "$owners" },
    {$group: { _id: "$owners", hosts: { $addToSet: "$hostname" } }},
    {$project : { _id: 0, owner: "$_id", hosts: 1 }}],
    function (err, owners) {
      if ( err ) {
        console.log(err)
        return next(err)
      }
      else if ( req.is('application/json') || req.xhr ) {
        res.json(owners)
      }
      else {
        res.render('servers-owners', { owners: owners })
      }
    })
}

// GET /servers/groups
exports.groups = function (req, res, next) {
  Server.aggregate([
  {$unwind: "$groups" },
  {$group: { _id: "$groups", hosts: { $addToSet: "$hostname" } }},
  {$project : { _id: 0, name: "$_id", hosts: 1 }},
  {$sort: { name: 1 }}],
  function (err, groups) {
    if ( err ) {
      console.log(err)
      return next(err)
    }
    else if ( req.is('application/json') || req.xhr ) {
      res.json(groups)
    }
    else {
      res.render('servers-groups', { groups: groups })
    }
  })
}

exports.ansible = function (req, res, next) {
  Server.find({status: { $ne: "unsupported" }}, function (err, servers) {
    if ( err ) {
      console.log(err)
      return res.send(500, err)
    }
    var groups = Object.create({}, {
      add: {
        value: function (grp, host) {
          if ( grp ) {
            if ( typeof this[grp] === 'undefined') {
              this[grp] = {}
            }
            this[grp][host] = 1
          }
        },
        enumerable: false
      }
    })
    if ( servers ) {
      servers.forEach ( function (s) {
        groups.add(s.os, s.hostname)
        groups.add(s.arch, s.hostname)
        groups.add(s.envType, s.hostname)
        groups.add(s.networkZone, s.hostname)
        s.owners.forEach ( function (user) {
         groups.add(user, s.hostname)
        })
        s.groups.forEach ( function (group) {
          groups.add(group, s.hostname)
        })
      })
    }
    var inventory = {}
    for (var grp in groups) {
      inventory[grp] = Object.keys(groups[grp])
    }
    res.json(inventory)
  })
}
