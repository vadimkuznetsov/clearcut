
route = {
  site: require('./site.js'),
  message: require('./message.js'),
  analytics: require('./analytics.js'),
  server: require('./server.js'),
  user: require('./user.js'),
  record: require('./record.js'),
}

module.exports = function (app) {
  app.get('/login', route.user.login)
  app.post('/login', route.user.signin)
  app.get('/logout', route.user.logout)

  app.all('*', route.user.requireAuthentication)

  app.get('/about', route.site.about)

  app.all('*', route.user.requireAuthorization)

  app.get('/', route.message.params, route.message.index)
  app.post('/', route.message.params, route.message.index)
  app.get('/host/:host', route.message.params, route.message.index)
  app.get('/facility/:facility', route.message.params, route.message.index)
  app.get('/priority/:priority', route.message.params, route.message.index)
  app.get('/program/:program', route.message.params, route.message.index)
  app.get('/owner/:owner', route.message.params, route.message.index)
  app.get('/start/:start', route.message.params, route.message.index)
  app.get('/end/:end', route.message.params, route.message.index)
  app.get('/range/:range', route.message.params, route.message.index)
  app.get('/all', route.message.all, route.message.params, route.message.index)
  app.get('/today', route.message.today, route.message.params, route.message.index)
  app.get('/yesterday', route.message.yesterday, route.message.params, route.message.index)
  app.get('/critical', route.message.critical, route.message.params, route.message.index)
  app.get('/failedauth', route.message.failedauth, route.message.params, route.message.index)

  app.get('/id/:id', route.message.id)

  app.get('/analytics/daily', route.analytics.daily)
  app.get('/analytics/daily/priority', route.analytics.bylevel)

  app.get('/server', route.server.servers)
  app.post('/server', route.server.createServer)
  app.get('/server/:host', route.server.serverByName)
  app.post('/server/:id', route.server.updateServer)
  app.delete('/server/:id', route.server.deleteServer)

  app.get('/servers/supported', route.server.supported)
  app.get('/servers/owners', route.server.owners)
  app.get('/servers/groups', route.server.groups)
  app.get('/servers/ansible', route.server.ansible)

  app.get('/records', route.record.records)
  app.post('/record', route.record.record)

  app.get('/account', route.user.account)
  app.post('/account', route.user.accountUpdate)
  app.delete('/account', route.user.accountDelete)
  app.get('/changePassword', route.user.editPassword)
  app.post('/changePassword', route.user.changePassword)
  app.post('/filter', route.user.filterUpdate)

  // app.all('*', user.requireAdmin)
  app.get('/user', route.user.requireAdmin, route.user.users)
  app.get('/user/:id', route.user.requireAdmin, route.user.userbyid)
  app.post('/user/:id', route.user.requireAdmin, route.user.update)
  app.delete('/user/:id', route.user.requireAdmin, route.user.delete)
}
