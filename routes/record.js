//
// routes/record.js
//

var mongoose = require('mongoose')
var Record   = require('../models/record')
var Server   = require('../models/server')

// POST /record
exports.record = function (req, res) {
  console.log(req.body)

  var record = new Record(req.body)
  record.save(function (err, savedRecord, numberSaved) {
    if (err) { res.send(err) }
    if (numberSaved != 1) { console.log("Nothing saved.")}
    console.log(savedRecord)
    res.send("201")
  })
}

// GET /records
// GET /records/?host=hostname
exports.records = function (req, res, next) {
  var conditions = {}
  if ( req.query.host ) {
    conditions.hostname = req.query.host
  }
  Record.find(conditions).sort("-created").find(function(err, records) {
    if ( err ) {
      console.log(err)
      return next(err)
    }
    console.log(records)
    if ( req.xhr ) {
      res.json(records)
    } else {
      res.render('records', { records: records })
    }
  })
}

// GET /record/:id
exports.recordbyid = function (req, res, next) {
  Record.findById(req.param('id'), function (err, record) {
    if ( err ) {
      console.log(err)
      return next(err)
    }
    console.log(record)
    res.render('record', { record: record })
  })
}
