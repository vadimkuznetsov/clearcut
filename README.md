# Clearcut.js

## Clearcut - a log viewer

Clearcut is a web application to view syslog-ng messages stored in MongoDB.

Application is written in JavaScript, using express, jade, less and mongoose ODM and Dojo toolkit.
