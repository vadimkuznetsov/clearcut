#!/usr/bin/env node

//
// (Re)set intial password
//

var mongoose = require('mongoose');
var User     = require('./models/user')
var config   = require('./config')
var passwd   = require('./lib/passwd')
var read     = require('read')

var db = mongoose.connect(config.mongoose_uri)

read({prompt: "Username: ", default: "admin" }, function (er, username) {
  read({prompt: "Password: ", default: "P@33w0rd", silent: true }, function (er, plaintext) {
    read({prompt: "Password again: ", default: "P@33w0rd", silent: true }, function (er, confirm) {
      if ( plaintext !== confirm) {
        console.log("Passwords do not match.")
        process.exit(1)
      }
      passwd.genPass(plaintext, function (err, pass) {
        User.update({username: username}, {$set: {password: pass}}, {upsert: true}, function(err, updated) {
          if (err) { 
            console.log(err) 
            process.exit(3)
          }
          if ( updated === 1 ) {
            console.log("password is changed.")
            process.exit(0)
          }
          else {
            console.log("password is not changed.")
            precess.exit(4)
          }
        })
      })
    })
  })
})
