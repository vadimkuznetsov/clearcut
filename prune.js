#!/usr/bin/env node

//
// Removes old data
//

var argv = require('optimist')
.usage('Removes old data.\nUsage: $0')
.alias('i', 'interval')
.describe('i', 'setInterval delay in milliseconds; Default: every hour.')
.default('i', 3600000)
.alias('a', 'days')
.default('a', 7)
.describe('a', 'Older then N days; Default: 7 days.')
.argv;

var mongoose = require('mongoose')
var config   = require('./config')
var Message  = require('./models/message')
var Analytics = require('./models/analytics')

var db = mongoose.connect(config.mongoose_uri)

function prune(days) {
  var now = Math.floor( Date.now() / 1000 ) 
  var then = now - days * 86400

  Message.remove({ts: {$lt: String(then) }}, function (err) {
    if (err) console.log(err)
  })
}

prune(argv.days)
setInterval(prune, argv.interval, argv.days)
