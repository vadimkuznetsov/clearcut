#!/usr/bin/env node

//
// cleracut.js
//   Will create express server
//
// Usage:
//   node clearcut.js
//

var express = require('express')
var less = require('less-middleware')
var mongoose = require('mongoose')
var https = require('https')
var http = require('http')
var path = require('path')
var util = require('util')
var MongoStore = require('connect-mongo')(express)
var config = require('./config')
var routes = require('./routes')
var pkg = require('./package.json')

var db = mongoose.connect(config.mongoose_uri)
var app = express()

app.configure(function () {
  app.set('port', process.env.CLEARCUT_PORT || config.server.port || 3456)
  app.set('host', process.env.CLEARCUT_HOST || config.server.host || '127.0.0.1')
  app.set('secure_port', process.env.CLEARCUT_SECURE_PORT || config.secure_server.port || 433 )
  app.set('secure_host', process.env.CLEARCUT_SECURE_HOST || config.secure_server.host || '0.0.0.0')

  app.set('views', __dirname + '/views')
  app.set('view engine', 'jade')

  app.enable('trust proxy')

  app.use(express.favicon())
  app.use(express.logger('short'))
  app.use(express.bodyParser())
  app.use(express.methodOverride())
  app.use(express.cookieParser())
  app.use(express.session({
    secret: config.cookie_secret,
    store: new MongoStore({
      url: config.mongoose_uri
    })
  }))

  app.use(function(req, res, next) {
    res.locals.session = req.session
    next()
  })

  app.use(less({
    dest: __dirname + '/public/css',
    src: __dirname + '/less',
    prefix: '/css',
    compress: true,
    once: true
  }))

  app.use('/css', express.static(__dirname + '/public/css'))
  app.use('/js', express.static(__dirname + '/public/js'))
  app.use('/img', express.static(__dirname + '/public/img'))

  app.use(app.router)

  app.use(express.errorHandler())

})

app.set('authn', config.authn)

app.locals({
  title: pkg.name,
  version: pkg.version,
})

// App routes
routes(app)

if ( config.secure_server && config.secure_server.tls) {
  https.createServer(config.secure_server.tls, app).listen(app.get('secure_port'), app.get('secure_host'), function(){
    util.log("HTTPS " + app.get('env') + " server listening on port " + app.get('secure_host') + ":" + app.get('secure_port'))
  })
}
if (config.server) {
  http.createServer(app).listen(app.get('port'), app.get('host'), function(){
    util.log("HTTP " + app.get('env') + " server listening on port " + app.get('host') + ":" + app.get('port'))
  })
}
