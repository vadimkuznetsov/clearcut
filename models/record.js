//
// Record model
//

var mongoose = require('mongoose')

var recordSchema = new mongoose.Schema({
  hostname: { type: String, lowercase: true, trim: true, index: true },
  created:  { type: Date, default: Date.now, expires: 60*60*24 },
  status:   { type: String, lowercase: true, trim: true, enum: ['success', 'failure'] },
  type:     { type: String, lowercase: true, trim: true, enum: ['sysinfo', 'update', 'tripwire'] },
  body:     String
})

module.exports = mongoose.model('Record', recordSchema)
