//models/analytics.js
//
//

var mongoose = require('mongoose')
var Mixed = mongoose.Schema.Types.Mixed

var analyticsSchema = new mongoose.Schema({
  _id:   Mixed,
  value: Number,
  lastrun: String
})

analyticsSchema.index({ "_id.d" : 1});
module.exports = mongoose.model('Analytics', analyticsSchema, 'analytics')
