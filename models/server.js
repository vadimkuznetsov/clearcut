//
// Server Model
//
var mongoose = require('mongoose')

var serverSchema = new mongoose.Schema({
  hostname:     { type: String, lowercase: true, trim: true },
  fqdn:         { type: String, lowercase: true, trim: true },
  ips:          [String],
  os:           { type: String, trim: true },
  arch:         { type: String, trim: true },
  desc:         String,
  envType:      { type: String, lowercase: true, trim: true },
  networkZone:  { type: String, set: capitalize, trim: true },
  owners:       [String],
  groups:       [String],
  class:        Number,
  loc:          { type: String, trim: true },
  status:       {
    type: String,
    lowercase: true,
    trim: true,
    enum: ['', 'active', 'unsupported'],
    index: true },
  seen:         Date,
  comments:     String,
  facts:        {}
})

serverSchema.index({ hostname: 1 }, { unique: true })
serverSchema.index({ fqdn: 1 })
serverSchema.index({ ips: 1 })
serverSchema.index({ os: 1 })
serverSchema.index({ arch: 1 })
serverSchema.index({ envType: 1 })
serverSchema.index({ networkZone: 1 })
serverSchema.index({ owners: 1 })
serverSchema.index({ groups: 1 })
serverSchema.index({ class: 1 })
serverSchema.index({ loc: 1 })

module.exports = mongoose.model('Server', serverSchema)

function capitalize (s) {
  if ('string' != typeof s) s = ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}
