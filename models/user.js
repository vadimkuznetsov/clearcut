//
// User Model
//
var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  username:     { type: String, lowercase: true, trim: true },
  password:     { salt: String, hash: String, changed: Date },
  fname:        String,
  lname:        String,
  email:        { type: String, lowercase: true, trim: true },
  created:      { type: Date, default: Date.now },
  lastlog:      Date,
  admin:        Boolean,
  locked:       Boolean,
  org:          String,
  ou:           String,
  location:     String,
  state:        String,
  country:      String,
  auth_id:      mongoose.Schema.Types.ObjectId,
  filters: [{
    name:       String,
    filter:     String,
  }]
});

userSchema.index({ username: 1 });
userSchema.index({ fname: 1 });
userSchema.index({ lname: 1 });
userSchema.index({ email: 1 });

var userModel = mongoose.model('User', userSchema);

module.exports = userModel;
