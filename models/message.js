//
// Message Model
//
var mongoose = require('mongoose');

var messageSchema = new mongoose.Schema({
  DATE:        String,
  HOST:        String,
  FACILITY:    String,
  PRIORITY:    String,
  PROGRAM:     String,
  MESSAGE:     String,
  ts:          String
});

messageSchema.index({ DATE: 1, ts: -1 });
messageSchema.index({ HOST: 1, ts: -1 });
messageSchema.index({ FACILITY: 1, ts: -1 });
messageSchema.index({ PRIORITY: 1,ts: -1 });
messageSchema.index({ PROGRAM: 1, ts: -1 });
messageSchema.index({ ts: -1 });
//messageSchema.index({ MESSAGE: text });

var messageModel = mongoose.model('Message', messageSchema);

module.exports = messageModel;
