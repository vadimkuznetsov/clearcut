//
// Authentication source Model
//
var mongoose = require('mongoose');

var authSchema = new mongoose.Schema({
  name:         String,
  url:          String,
  base_dn:      String,
  attr_login:   String,
  attr_fname:   String,
  attr_lname:   String,
  attr_mail:    String,
  register:     Boolean
})

var authModel = mongoose.model('Auth', authSchema);

module.exports = authModel;
